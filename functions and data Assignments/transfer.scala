package function.scala

class Account(n:Int,b:Double)
{
  val AccountNumber:Int=n
  var AccountBalance:Double=b

  def send(a:Double)=this.AccountBalance=this.AccountBalance-a
  def receive(a:Double)=this.AccountBalance=this.AccountBalance+a


  override def toString=AccountNumber+" "+AccountBalance
  }
object transfer {
  def main(args:Array[String]):Unit={
    def transfer(a:Account,b:Account,amount:Double)= {
      a.send(amount)
      b.receive(amount)
    }


      println("Enter account number and balance:")
      val n1=scala.io.StdIn.readInt()
      val b1=scala.io.StdIn.readDouble()

      println("Enter the second account number and balance:")
      val n2=scala.io.StdIn.readInt()
      val b2=scala.io.StdIn.readDouble()

      println("Enter the transfer money:")
      val amount=scala.io.StdIn.readInt()
      val x=new Account(n1:Int,b1:Double)
      val y=new Account(n2:Int,b2:Double)
      println("Before Transfer:"+x+" "+y)
      transfer(x,y,amount)
      println("After Transfer:"+x+" "+y)
    }
}
