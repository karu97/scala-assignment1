package function.scala
object Bank {
  var bank:List[Account]=List()

  class Account(id:String, n:Int, b:Double)
  {
    val nic:String=id
    val acnumber:Int=n
    var balance:Double=b

    def withdraw(a:Double)=this.balance=this.balance-a
    def deposit(a:Double)=this.balance=this.balance+a
    def transfer(a:Account,b:Double)={
      this.withdraw(b)
      a.deposit(b)
    }

    override def toString="["+nic+":"+acnumber+":"+balance+"]"
  }
  def main(args:Array[String]): Unit = {
    var acc1 = new Account("12345", 101, 30000.00)
    var acc2 = new Account("13452", 102, 25000.00)
    var acc3 = new Account("14523", 103, 100000.00)
    var acc4 = new Account("15234", 104, -7200.00)
    var acc5 = new Account("21345", 105, -9100.00)
    var acc6 = new Account("23451", 106, 95000.00)

    var bank: List[Account] = List(acc1, acc2, acc3, acc4, acc5, acc6)
    val overdraft = (b: List[Account]) => b.filter(x => x.balance < 0)
    val balance = (b: List[Account]) => b.reduce((x1, x2) => new Account(" ", 0, x1.balance + x2.balance)).balance
    val interest = (b: List[Account]) => b.map(x => new Account(x.nic, x.acnumber, if (x.balance < 0) x.balance * 1.1 else x.balance * 1.05))

    println("List of accounts with negative balance:" + overdraft(bank))
    println("Sum of the account balances:" + balance(bank))
    println("Final balances after applying interests:" + interest(bank))


  }

  }
