package function.scala

class rationalNeg(n:Int,d:Int) {
  require(d > 0, "d must be greater than zero")

  def numer = n / gcd(Math.abs(n), d)
  def denom = d / gcd(Math.abs(n), d)
  def this(n: Int) = this(n, 1)

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  def neg = new Rational(-this.numer, this.denom)
  override def toString = numer + "/" + denom
}

  object rationalNeg {
    def main(args:Array[String]): Unit ={
      val r1=new Rational(1,4)
      println(r1.neg)
    }
  }
