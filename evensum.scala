package recursive.scala

object evensum extends App{
  def sum(x:Int):Int=x match{
    case x if x==0 =>0
    case x if x%2==0 =>x+sum(x-1)
    case x => sum(x-1)
  }
  def readIntp(s:String)={
    println(s);
    scala.io.StdIn.readInt()
  }
  var x=readIntp("Enter the number:")
  println(sum(x))
}
