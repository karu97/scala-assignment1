package recursive.scala

object primeseq extends App{
  def prime(p:Int,x:Int=2):Any=x match{
    case x if x==p => p
    case x if gcd(p,x)>1 =>" "
    case x =>prime(p,x+1)
  }
  def gcd(a:Int,b:Int):Int=b match{
    case 0 =>a
    case b if b>a =>gcd(b,a)
    case _ =>gcd(b,a%b)
  }
  def readIntp(s:String)={
    println(s);
    scala.io.StdIn.readInt()
  }
  def primeseq(x:Int):Any={
    println(prime(x))
    if(x>0) primeseq(x-1)
  }
  var x=readIntp(("enter the number:"))
  primeseq(x)

}
