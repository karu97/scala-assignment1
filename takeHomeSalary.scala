package cost

object takeHomeSalary extends App{
  def wage(h:Int)=h*150
  def ot(h:Int)=h*75
  def salary(h1:Int,h2:Int)=wage(h1)+ot(h2)
  def tax(x:Int)=x*0.1
  def takeHome(h1:Int,h2:Int)=salary(h1,h2)-tax(salary(h1,h2))
  def readIntP(s:String)={
    println(s);
    scala.io.StdIn.readInt()
  }
  var h1=readIntP("Enter the normal hours:")
  var h2=readIntP("Enter the ot hours:")
  println(takeHome(h1,h2))
}