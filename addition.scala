package recursive.scala

object addition extends App{
  def addition(x:Int):Int=x match{
    case x if x==0 =>0
    case x =>x+addition(x-1)
  }
  def readIntp(s:String)={
    println(s);
    scala.io.StdIn.readInt()
  }
  var x=readIntp("enter the number:")
  println(addition(x))
}
