package mapreduce.scala

object caesarcipher extends App{

    val alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "
    val n=(scala.io.StdIn.readLine("Shift by: ").toInt+alphabet.size)%alphabet.size
    val text=scala.io.StdIn.readLine("Enter the string: ")


    val E = (c: Char, key: Int, a: String) => a((a.indexOf(c.toUpper) + key) % a.size)
    val D = (c: Char, key: Int, a: String) => a((a.indexOf(c.toUpper) - key) % a.size)

    val cipher = (algo: (Char, Int, String) => Char, s: String, key: Int, a: String) => s.map(algo(_, key, a))
    val ct = cipher(E, text, n, alphabet)
    val pt = cipher(D, ct, n, alphabet)

    println(ct)
    println(pt)


}
