package caseClass.scala
import math.{sqrt,pow}
case class Point(x:Int,y:Int)
{
  def +(that:Point)=Point(this.x+that.x,this.y+that.y)
  def move(dx:Int,dy:Int)=Point(this.x+dx,this.y+dy)
  def distance(p:Point)=sqrt(pow(x-p.x,2)+pow(y-p.y,2))
  def invert(x:Int,y:Int)=Point(y,x)
}
object Point extends App{
  val p0=Point(0,0)
  val p1=Point(0,3)
  val p2=Point(5,8)
  val p3=p1.move(2,3)
  val d=p1.distance(p2)
  val i=p0.invert(2,3)
  println(p1)
  println(p2)
  println("sum of p1,p2:"+(p1+p2))
  println("moved point of p1 by (2,3) :"+p3)
  println("distance between p1,p2: "+d)
  println("inverted point of (2,3): "+i)

}
