package recursive.scala

object iseven extends App{
  def isEven(x:Int):Boolean=x match{
    case 0 =>true
    case _ =>isOdd(x-1)
  }

  def isOdd(x:Int):Boolean = !(isEven(x))
  def readIntp(s:String)={
    println(s);
    scala.io.StdIn.readInt()
  }
var x=readIntp("Enter a number:")
  println(isEven(x))
  println(isOdd(x))

}
