package recursive.scala

object recursive extends App{
 def prime(p:Int,x:Int=2):Boolean= x match{
   case x if(x==p) =>true
   case x if gcd(p,x)>1 =>false
   case x =>prime(p,x+1)
 }
  def gcd(a:Int,b:Int):Int=b match{
    case 0 =>a
    case b if b>a =>gcd(b,a)
    case _ =>gcd(b,a%b)
  }
  def readIntP(s:String)={
    println(s);
    scala.io.StdIn.readInt()
  }
  var p=readIntP(("Enter the number"))
  println(prime(p))
}
